var map;
var baseMaps = {};
var overlayMaps = {};


function addToLayerControl(name, layer){
    this.overlayMaps[name] = layer;
}

function LoadMap(){
    
    //Instancia del mapa con la configuración básica
    this.map = L.map('map', { zoomControl:true, maxZoom:28, minZoom:1});
    this.map.setView([40.0000000, -4.000000000], 6); //Vista por defecto del mapa de España con un zoom de 6

    
    //Mapa BASE
    var basemapTile = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {});
    
    this.baseMaps = {"<span class='capas_texto capas_titulo'><i class='fa fa-globe p-1 h5'></i>Mapa base</span>": basemapTile};
    this.map.addLayer(basemapTile);
    
    
    //Capas de consulta
    
    
    //Capa de zonas afectadas por incendio en Marxuquera 2018
	var incendio = new L.Shapefile('data/incendio.zip', {
        importUrl: "js/shp.js",
        customOption: 4,
		onEachFeature: forEachFeature
		})//.addTo(map);
    
    
    //Capa de contorno de temperaturas
	var temp_contorno = new L.Shapefile('data/temp_contorno.zip', {importUrl: "js/shp.js", customOption: 3});

    
        /* Raster de temperaturas */
        d3.request("data/temperatura.tif").responseType('arraybuffer').get(
            function (error, tiffData) {
                // Temperature (BAND 1)                
                let t = L.ScalarField.fromGeoTIFF(tiffData.response, bandIndex = 0);
                scale = chroma.scale(['rgba(0,0,0,0)','rgb(235,0,0)','orange','yellow','rgb(181, 214, 47)','rgb(120, 250, 47)']);
                scale.domain([0,100,200,250,254,255]);
                
                let layerT = L.canvasLayer.scalarField(t, {
                    color: scale,
                    opacity: 1,
                    customOption: 1
                });
                layerT.on('click', function (e) {
                    if (e.value !== null) {
                        let v = e.value.toFixed(1);
                        //Calculo de la temperatura
                            var t1 = ((23*169)/v).toFixed(2); //calibración de la temperatura


                        let html = (`<span class="popupText">Temperature media ${t1} ºC</span>`);
                        let popup = L.popup()
                            .setLatLng(e.latlng)
                            .setContent(html)
                            .openOn(map);
                    }
                });
                
                addToLayerControl("<span class='capas_texto'><i class='fas fa-image'></i>Temperaturas medias Junio (2007)</span>", layerT);

            });
    
    
        /* Raster de zonas afectadas */
        d3.request("data/zonas_afectadas.tif").responseType('arraybuffer').get(
            function (error, tiffData) {
                // Temperature (BAND 1)                
                let t = L.ScalarField.fromGeoTIFF(tiffData.response, bandIndex = 0);
                scale = chroma.scale(['rgba(0,0,0,0)','rgba(0,0,0,0)','white','grey','black']);
                scale.domain([0,5,7,80,255]);
                let layerT = L.canvasLayer.scalarField(t, {
                    color: scale,
                    opacity: 1,
                    customOption: 2
                });
                
                addToLayerControl("<span class='capas_texto'><i class='fas fa-image'></i>Marxuquera y alrededores, zonas afectadas incendio (2018)</span>", layerT); //Raster de zonas afectadas y alrededores

                addToLayerControl("<span class='capas_texto'><i class='fas fa-vector-square'></i>Contorno afecciones incendio Marxuquera (2018)</span>", incendio); //Vectoriales de la zona afectada por el incendio
                addToLayerControl("<span class='capas_texto'><i class='fas fa-vector-square'></i>Contorno temperaturas medias Junio (2007)</span>", temp_contorno); //Se añade el vector por encima de la capa raster


                //Control de capas ~
                var control = L.control.layers(baseMaps,overlayMaps,{
                    sortLayers:true,
                    sortFunction: function (a, b) {
                        return a.options.customOption - b.options.customOption;
                    }
                });

                control.addTo(map); //Refresca el control de capas al añadir el último raster

                //Todo cargado
                document.getElementById("loading").style.display = "none";

            });
    
    

        //Carga Eventos y configuraciones extras
        LoadEvents();
    
}


/*
 * Carga eventos sobre las capas y herramientas de funcionamiento extra
 */
function LoadEvents(){
    

    
    
    L.control.scale({imperial:false}).addTo(map); //Añade la escala

        //Añade un control de minimapa
        var base2 = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {minZoom: 0, maxZoom: 13});
		var miniMap = new L.Control.MiniMap(base2, { toggleDisplay: true }).addTo(map);

        //Añade un control para realizar medidas y se configura con unidades del Sistema internacional
        var measureControl = L.control.measure({
            position: 'topleft',
            primaryLengthUnit: 'kilometers',
            secondaryLengthUnit: 'meters',
            primaryAreaUnit: 'hectares',
            secondaryAreaUnit: 'sqmeters',
            decPoint: ',',
            thousandsSep: '.'

        });
        measureControl.addTo(map);


    //
    var printer = L.easyPrint({
      		tileLayer: baseMaps[0],
      		sizeModes: ['Current', 'A4Landscape', 'A4Portrait'],
      		filename: 'Mi visor',
      		exportOnly: true,
      		hideControlContainer: true
		}).addTo(map);

		function manualPrint () {
			printer.printMap('CurrentSize', 'MyManualPrint')
		}
    

}


function forEachFeature(feature, layer) {
	   if (feature.properties) {
                layer.bindPopup(Object.keys(feature.properties).map(function(k) {
                    return k + ": " + feature.properties[k];
                }).join("<br />"), {maxHeight: 250 });
    }
}
